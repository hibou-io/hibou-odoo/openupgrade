FROM registry.gitlab.com/hibou-io/hibou-odoo/suite:12.0

USER 104
COPY --chown=104 . /opt/odoo/odoo

USER 0
RUN set -x; cd /opt/odoo/odoo \
    && cp -R ./odoo $(pip list -v | grep odoo | grep -o '/.*$')

USER 104


